export interface User {
    uid?: string,
    name?: string,
    cpf?: string,
    email?: string,
    lastname?: string,
    password?: string
}