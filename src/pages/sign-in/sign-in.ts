import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { PreferenceProvider } from '../../providers/preference/preference';
import { OverlayProvider } from '../../providers/overlay/overlay';
import { User } from '../../models/user';

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {

  private userData:User= {
    name: "Bruno",
    cpf: '06345645689',
    email: 'daniel@email.com',
    lastname: "Santos",
    password: "123456"

  }

  private load:Loading;

  loginErrFirebase = [
    {erro: 'auth/invalid-email', msg: 'O login é inválido'},
    {erro: 'auth/user-disabled', msg: 'Login fornecido desativado.'},
    {erro: 'auth/user-not-found', msg: 'Login ou senha inválidos'},
    {erro: 'auth/wrong-password', msg: 'Login ou senha inválidos'},
    {erro: 'auth/network-request-failed', msg: 'Verifique sua conexão a internet e tente novamente'}
  ]


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authProv: AuthProvider,
    public preferenceProv: PreferenceProvider,
    public overlayProv: OverlayProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignInPage');
  }

  public SingInTeste() {
    this.load = this.overlayProv.loadPresent();
    this.authProv.signIn(this.userData).then((data) => {
      console.log('return signin', data)
      return this.authProv.getDataUserInFirebase(data.user.uid).then((user) => {
        console.log('return getUserFirebase', user)
        this.preferenceProv.create('user', user).then(() => {
          this.load.dismiss();
          this.navCtrl.setRoot('HomePage');
        })
      })
    }).catch((err:any) => {
      console.error(err)
      console.log('error this.authProv.signIn')
      if(err.code == 'auth/user-not-found') {
        console.log('cadastrar usuario')
        this.authProv.singUp(this.userData).then(() => {
          console.log('this.authProv.singUp then', this.authProv.user)
          return this.preferenceProv.create('user', this.authProv.user).then(() => {
            this.load.dismiss()
            this.navCtrl.setRoot('HomePage')
          })
        }).catch((err) => {
          console.warn(err)
          this.load.dismiss();
          this.overlayProv.alertPresent({message: err.code});
        })
      } else {
        console.error('não cadastrar')
        this.load.dismiss()
        let errorMsg = this.loginErrFirebase.find(item => item.erro == err.code);
        if(errorMsg) {
          this.overlayProv.alertPresent({message: errorMsg.msg})
        } else {
          this.overlayProv.alertPresent({message: 'falha ao tentar logar com os dados fornecidos'})
        }
      }
    })
  }





}
