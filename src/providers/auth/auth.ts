import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { User } from '../../models/user';
import { PreferenceProvider } from '../preference/preference';

@Injectable()
export class AuthProvider {

  public user:User;

  constructor(
    private auth: AngularFireAuth,
    private afd: AngularFireDatabase,
    private preferenceProv: PreferenceProvider
  ) { }

  public signIn(userData:User):Promise<any> {
    return this.auth.auth.signInWithEmailAndPassword(userData.email, userData.password)
  }

  public singUp(userData:User):Promise<any> {
    return this.auth.auth.createUserWithEmailAndPassword(userData.email, userData.password).then((data) => {
      console.log('data.user.uid', data.user.uid)
      this.user = {
        ...userData,
        uid: data.user.uid,
        password: ''
      }
      console.log('this.user', this.user)
      return this.afd.database.ref('users').child(data.user.uid).set({
        uid:data.user.uid,
        cpf: userData.cpf,
        email: userData.email,
        name: userData.name,
        lastname: userData.lastname
      })
    })

  }

  public getDataUserInFirebase(uid):Promise<any> {
    return new Promise((resolve, reject ) => {
      this.afd.database.ref('users').child(uid).on('value', (snapshot) => {
        console.log(snapshot.val())
        if(snapshot.exists()){
          resolve(snapshot.val())
        } else {
          reject('erro/datanotfound')
        }
      })
    })
  }

  public logout() {
    return this.preferenceProv.remove('user');
  }

}
