
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class PreferenceProvider {

  constructor(
    private storage:Storage,
    ) {

  }

  public create(key:string, value:any): Promise<any> {
    return this.storage.ready()
    .then(() => { 
      return this.storage.set(key, value)
    })  
  }

  public get(key): Promise<any> {
    return this.storage.ready()
    .then(() => { 
      return this.storage.get(key)
    })
  }

  public remove(key): Promise<boolean> {
    return this.storage.ready()
      .then(() => {
        return this.storage.remove(key)
      })
  }


}
