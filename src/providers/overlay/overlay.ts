import { Injectable } from '@angular/core';
import { ToastController, LoadingController, AlertController, Loading } from 'ionic-angular';

@Injectable()
export class OverlayProvider {


  constructor(
    private toastCtrl:ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) { }

  public toastPresent(opts:{message:string, duration?:number, closeButtonText?: string, dismissOnPageChange?:boolean}):void{
    this.toastCtrl.create({
      showCloseButton: true,
      duration: opts.duration? opts.duration : 10000,
      dismissOnPageChange: false,
      closeButtonText: opts.closeButtonText? opts.closeButtonText : 'OK',
      message: opts.message
    }).present();
  }

  public loadPresent(content?:string):Loading {
    let load = this.loadingCtrl.create({
      content: content? content: 'aguarde...'
    })

    load.present();

    return load;
  }

  public alertPresent(opts:{title?:string, message:string}):void {
    this.alertCtrl.create({
      title: opts.title? opts.title: 'Alerta',
      message: opts.message,
      buttons: ['OK']
    }).present();
  }

  public presentConfirm(opts:{title:string, message:string, text_confirmation?:string}):Promise<boolean> {

    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create({
        title: opts.title,
        message: opts.message,
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
              reject(false);
            }
          },
          {
            text: opts.text_confirmation?opts.text_confirmation: 'OK',
            handler: () => {
              console.log('OK clicked');
              resolve(true);
            }
          }
        ]
      });
      alert.present();
    });
    
  }

}
